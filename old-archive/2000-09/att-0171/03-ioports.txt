0000-001f : dma1
0020-003f : pic1
0040-005f : timer
0060-006f : keyboard
0070-007f : rtc
0080-008f : dma page reg
00a0-00bf : pic2
00c0-00df : dma2
00f0-00ff : fpu
0170-0177 : ide1
01f0-01f7 : ide0
02f8-02ff : serial(auto)
0376-0376 : ide1
0378-037f : parport0
03c0-03df : vga+
03f6-03f6 : ide0
03f8-03ff : serial(auto)
0778-077a : parport0
9000-9007 : ide0
9008-900f : ide1
9800-9807 : ide2
9c02-9c02 : ide2
a000-a007 : ide2
a010-a0ff : HPT366
ac00-ac07 : ide3
ac10-acff : HPT366
b800-b80f : ESS Solo-1 SB
bc00-bc0f : ESS Solo-1 VC (DMA)
c000-c003 : ESS Solo-1 MIDI
c400-c403 : ESS Solo-1 Game port
c800-c83f : atp870u

