
Hello,

 I have, for personal reasons, had to invest some time into getting SANE
network scanning to work for Windows clients.
WinSane would not work for me, it crashed every time.

I attach the result, which is a TWAIN data source that can access SANE
backends. Unlike WinSane, this one is open source!
It works quite well on my SnapScan e20, other's mileage may vary. It's all
in German, too, but there isn't much text, so translation should be easy.
I would encourage anyone willing to do some work on it, to make it into the
universal tool it could be. I don't have time to do further development on
it.

Now, here's the legal bit:
The code included is in part written by others:
The TWAIN driver skeleton is from the Twain Working Group, and copyrights
may apply.
Some of the image manipulation is from liblug, which is included as part of
the source tree. It is open source, requiring only the copyright to be
present in the source code.
Part, of course, is SANE stuff itself.
It uses the scanimg.exe binary from WinSane, so that needs to be present.
All other code is mine, and can be copied, developed and deployed as anyone
may see fit. I would however like to have it evolve into a standard frontend
for Windows scanning via SANE.

Features are: Preview scan, zoom preview, selection of resolution and color
depth, image adjustments (gamma, brightness, sharpen, blur) and transfer via
file, memory and Windows native mode.
This is by no means clean code, much is redundant, useless or inefficient.
Be welcome to make any changes to improve it.
In scanner.c, the SANE backend is hardcoded. That would need to be changed
first, I'd guess.
Gray scaled bitmaps are transferred to the application as RGB. This is
needed because Corel Photo Paint and CorelDraw will not work when paletted
images are transferred, God only knows why.
Because of that, there is a lot of hard to understand code in scanner.c, but
that part is working perfectly, so it may be good to leave that alone at
first.
B/W scanning may need to take the PIXELFLAVOR setting into account,
currently is does not.

To get it to build, you will need:
MSVC 6.0 Professional Edition
WinSane (for scanimg.exe and sanedll.dll)
The Twain Working Group driver skeleton (maybe)

If you cannot compile without the Driver toolkit from Twain Working Group,
you will need to unzip the attached file in such a way that the "SANE
Driver" directory becomes a sibling of the Twain Working Group's driver
toolkit. Then, compile the toolkit version of the driver and delete the .ds
file from the c:windows\Twain_32\Sample directory.

When compiling, a directory c:\twain will be created. There you will find
the data source (.ds) file. Place that in c:\windows\twain_32, along with
scanimg.exe and sanedll.dll.

That should be all that's needed.

I'm sorry I can't make this any easier, but I'm not an expert at Windows
application packaging. Maybe someone else can make a nicer package from it.

Best regards,

Mike

